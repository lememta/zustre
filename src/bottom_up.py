from LogManager import LoggingManager
import z3
import z3core
import stats

from z3_utils import *

class BottomUp(object):


    def __init__(self, fp):
        self.fp = fp
        self._log = LoggingManager.get_logger(__name__)
        return

    def run(self):
        self._log.info("Running Bottom Up")
        answer =  self.fp.get_answer()
        print answer
        lemma = self.fp_get_cover_delta(answer)
        print lemma
        return

    def fp_get_cover_delta (self, pred, level=-1):
        sub = []
        for i in range (0, pred.num_args ()):
            sub.append (pred.arg (i))

        lemma = self.fp.get_cover_delta (level, pred.decl ())
        if z3core.Z3_get_bool_value (self.fp.ctx.ctx, lemma.as_ast ()) != z3.unsat:
            lemma = z3.substitute_vars (lemma, *sub)
        return lemma
